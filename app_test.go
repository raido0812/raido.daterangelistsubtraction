package daterangesubtractor

import (
	"fmt"
	"testing"
	"time"
)

// constants for testing purposes
const HardcodedYear = 2019
const HardcodedMonth = time.January
const HardcodedDay = 1
const DateFormat = "15:04:05"

type DateRangeTestCase struct {
	name               string
	baseDateRanges     []DateRange
	subtractDateRanges []DateRange
	expectedDateRanges []DateRange
}

var dateRangeTestCases = []DateRangeTestCase{
	{
		"(9:00-10:00) “minus” (9:00-9:30) = (9:30-10:00)",
		[]DateRange{
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 0, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 10, 0, 0, 0, time.UTC),
			},
		},
		[]DateRange{
			DateRange{
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 0, 0, 0, time.UTC),
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 30, 0, 0, time.UTC),
			},
		},
		[]DateRange{
			DateRange{
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 30, 0, 0, time.UTC),
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 10, 0, 0, 0, time.UTC),
			},
		},
	},
	{
		"(9:00-10:00) “minus” (9:00-10:00) = ()",
		[]DateRange{
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 0, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 10, 0, 0, 0, time.UTC),
			},
		},
		[]DateRange{
			DateRange{
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 0, 0, 0, time.UTC),
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 10, 0, 0, 0, time.UTC),
			},
		},
		[]DateRange{},
	},
	{
		"(9:00-9:30) “minus” (9:30-15:00) = (9:00-9:30)",
		[]DateRange{
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 0, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 30, 0, 0, time.UTC),
			},
		},
		[]DateRange{
			DateRange{
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 30, 0, 0, time.UTC),
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 15, 0, 0, 0, time.UTC),
			},
		},
		[]DateRange{
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 0, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 30, 0, 0, time.UTC),
			},
		},
	},
	{
		"(9:00-9:30, 10:00-10:30) “minus” (9:15-10:15) = (9:00-9:15, 10:15-10:30)",
		[]DateRange{
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 0, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 30, 0, 0, time.UTC),
			},
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 10, 0, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 10, 30, 0, 0, time.UTC),
			},
		},
		[]DateRange{
			DateRange{
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 15, 0, 0, time.UTC),
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 10, 15, 0, 0, time.UTC),
			},
		},
		[]DateRange{
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 0, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 15, 0, 0, time.UTC),
			},
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 10, 15, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 10, 30, 0, 0, time.UTC),
			},
		},
	},
	{
		"(9:00-11:00, 13:00-15:00) “minus” (9:00-9:15, 10:00-10:15, 12:30-16:00) = (9:15-10:00, 10:15-11:00)",
		[]DateRange{
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 0, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 11, 0, 0, 0, time.UTC),
			},
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 13, 0, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 15, 0, 0, 0, time.UTC),
			},
		},
		[]DateRange{
			DateRange{
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 0, 0, 0, time.UTC),
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 15, 0, 0, time.UTC),
			},
			DateRange{
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 10, 0, 0, 0, time.UTC),
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 10, 15, 0, 0, time.UTC),
			},
			DateRange{
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 12, 30, 0, 0, time.UTC),
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 16, 0, 0, 0, time.UTC),
			},
		},
		[]DateRange{
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 15, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 10, 0, 0, 0, time.UTC),
			},
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 10, 15, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 11, 0, 0, 0, time.UTC),
			},
		},
	},
	{
		"(9:00-11:00, 13:00-15:00) “minus” (9:15-9:30, 10:00-10:15, 12:30-14:30) = (9:00-9:15, 9:30-10:00, 10:15-11:00, 14:30-15:00)",
		[]DateRange{
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 0, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 11, 0, 0, 0, time.UTC),
			},
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 13, 0, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 15, 0, 0, 0, time.UTC),
			},
		},
		[]DateRange{
			DateRange{
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 15, 0, 0, time.UTC),
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 30, 0, 0, time.UTC),
			},
			DateRange{
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 10, 0, 0, 0, time.UTC),
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 10, 15, 0, 0, time.UTC),
			},
			DateRange{
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 12, 30, 0, 0, time.UTC),
				time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 14, 30, 0, 0, time.UTC),
			},
		},
		[]DateRange{
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 0, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 15, 0, 0, time.UTC),
			},
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 9, 30, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 10, 0, 0, 0, time.UTC),
			},
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 10, 15, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 11, 0, 0, 0, time.UTC),
			},
			DateRange{
				start: time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 14, 30, 0, 0, time.UTC),
				end:   time.Date(HardcodedYear, HardcodedMonth, HardcodedDay, 15, 0, 0, 0, time.UTC),
			},
		},
	},
}

func TestAllScenarios(t *testing.T) {
	for _, testcase := range dateRangeTestCases {
		fmt.Printf("Starting TestCase:%s", testcase.name)
		fmt.Println()
		t.Run(testcase.name, testDateRangeSubtract(testcase.baseDateRanges, testcase.subtractDateRanges, testcase.expectedDateRanges))
	}
}

func testDateRangeSubtract(baseDateRanges []DateRange, subtractDateRanges []DateRange, expectedDateRanges []DateRange) func(*testing.T) {
	return func(t *testing.T) {
		// Arrange
		// skipped as it will come in via test table

		// Action
		actualDateRanges := SubtractDateRanges(baseDateRanges, subtractDateRanges)

		// Assert
		if len(actualDateRanges) != len(expectedDateRanges) {
			t.Errorf("Expected the %d number of date ranges but instead got %d!", len(expectedDateRanges), len(actualDateRanges))
		} else {
			for i, actual := range actualDateRanges {
				if !actual.start.Equal(expectedDateRanges[i].start) {
					t.Errorf("Output for item %d Incorrect. Expected start time %s but Got %s", i+1, expectedDateRanges[i].start.Format(DateFormat), actual.start.Format(DateFormat))
				}
				if !actual.end.Equal(expectedDateRanges[i].end) {
					t.Errorf("Output for item %d Incorrect. Expected end time %s but Got %s", i+1, expectedDateRanges[i].end.Format(DateFormat), actual.end.Format(DateFormat))
				}
			}
		}
	}
}
