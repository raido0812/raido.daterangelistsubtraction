# Purpose
This Code repository serves as the result of completing an Interview Homework Question

## Language
As requested, work was done in [Go](https://golang.org/). This would be the first time I've worked with Golang, so there may be other data types more suited for this problem that I'm not aware of.

## Execution
The core logic is implmented in 1 function with unit tests to test various scenarios to ensure the function is working correctly.

To run the tests, run the following on the command line
```
go test
```

## Interview Question as received
### Intructions:
Write a program that will subtract one list of time ranges from another. 

### Formally:
For two lists of time ranges A and B, a time is in (A-B) if and only if it is part of A and not part of B.
A time range has a start time and an end time. You can define times and time ranges however you want (Unix timestamps, date/time objects in your preferred language, the
actual string “start-end”, etc).

Your solution shouldn’t rely on the granularity of the timestamps (so don’t, for example,
iterate over all the times in all the ranges and check to see if that time is “in”).

### Examples:
- (9:00-10:00) “minus” (9:00-9:30) = (9:30-10:00)
- (9:00-10:00) “minus” (9:00-10:00) = ()
- (9:00-9:30) “minus” (9:30-15:00) = (9:00-9:30)
- (9:00-9:30, 10:00-10:30) “minus” (9:15-10:15) = (9:00-9:15, 10:15-10:30)
- (9:00-11:00, 13:00-15:00) “minus” (9:00-9:15, 10:00-10:15, 12:30-16:00) = (9:15-10:00, 10:15-11:00)