package daterangesubtractor

import (
	"sort"
	"time"
)

// DateRange Represents a range between two isntances in time
type DateRange struct {
	start time.Time
	end   time.Time
}

// SubtractDateRanges takes 2 arrays of DateRange and removes any overlap from the first list of DateRange
func SubtractDateRanges(baseDateRanges []DateRange, subtractDateRanges []DateRange) []DateRange {
	// Below logic assumes that baseDateRanges doesn't have overlapping ranges

	// Logic is basically modelled similarly to a merge join.
	// we dequeue the items in each array/slice, based on various comparisons,
	// decide whether to add an item to the result, or to mutate the working set for further work
	// and keep going until the queue of data is exhausted.

	// being on GO, there was potential to model the 2 arrays as channels, but then there'd be no guarantee the the items are coming in order
	// much of the below logic can be refactored for easier reading and understanding. Could benefit from a more sophisticated queue or linked list

	// As with merge join, below would only work when both collections are sorted
	sort.Slice(baseDateRanges, func(i, j int) bool {
		return baseDateRanges[i].start.Before(baseDateRanges[j].start)
	})

	sort.Slice(subtractDateRanges, func(i, j int) bool {
		return subtractDateRanges[i].start.Before(subtractDateRanges[j].start)
	})

	var Result []DateRange
	for len(baseDateRanges) > 0 && len(subtractDateRanges) > 0 {

		isBaseStartOnOrAfterSubtractStart := baseDateRanges[0].start.Sub(subtractDateRanges[0].start).Seconds() >= 0
		isBaseStartOnOrAfterSubtractEnd := baseDateRanges[0].start.Sub(subtractDateRanges[0].end).Seconds() >= 0
		isBaseEndOnOrAfterSubtractEnd := baseDateRanges[0].end.Sub(subtractDateRanges[0].end).Seconds() >= 0

		isSubtractStartOnOrAfterBaseStart := subtractDateRanges[0].start.Sub(baseDateRanges[0].start).Seconds() >= 0
		isSubtractStartOnOrAfterBaseEnd := subtractDateRanges[0].start.Sub(baseDateRanges[0].end).Seconds() >= 0
		isSubtractEndOnOrAfterBaseEnd := subtractDateRanges[0].end.Sub(baseDateRanges[0].end).Seconds() >= 0

		// base doesn't overlap with anything to subtract, and append base to result and move on
		if isSubtractStartOnOrAfterBaseEnd {
			Result = append(Result, baseDateRanges[0])
			baseDateRanges = baseDateRanges[1:]
			continue
		}

		// item to subtract is already in past, can pop it and continue
		if isBaseStartOnOrAfterSubtractEnd {
			subtractDateRanges = subtractDateRanges[1:]
			continue
		}

		// if it reaches here, there's some form of overlap, in this case depending on overlap, we can mutate the base to be subtracted by next date range until there's nothing to subtract

		// scenario 1: start before the base and ends somewhere within base range
		if isBaseStartOnOrAfterSubtractStart &&
			!isBaseStartOnOrAfterSubtractEnd &&
			isBaseEndOnOrAfterSubtractEnd {

			// removing overlap means the start of base needs to be pushed to where the subtract end time is
			baseDateRanges[0].start = subtractDateRanges[0].end

			// we've handled the overlap on this range, can pop it out from processing
			subtractDateRanges = subtractDateRanges[1:]

			// if start meets end, this range is useless and can be popped
			if baseDateRanges[0].start.Equal(baseDateRanges[0].end) {
				baseDateRanges = baseDateRanges[1:]
			}

			continue
		}

		// scenario 2: start after the base and ends outside base range
		if isSubtractStartOnOrAfterBaseStart &&
			!isSubtractStartOnOrAfterBaseEnd &&
			isSubtractEndOnOrAfterBaseEnd {
			// removing overlap means the end of base needs to be pushed to where the subtract start time is
			baseDateRanges[0].end = subtractDateRanges[0].start
			continue
		}

		// scenario 3: start before the base and ends outside base range
		if isBaseStartOnOrAfterSubtractStart &&
			isSubtractEndOnOrAfterBaseEnd {
			// base is completely covered by the subtract range, entire base can be thrown away
			baseDateRanges = baseDateRanges[1:]
			continue
		}

		// scenario 4: start and end within the base range
		if isSubtractStartOnOrAfterBaseStart &&
			isBaseEndOnOrAfterSubtractEnd {
			// the subtract range exists completely within bounds of base range, base range is effectively split in 2 for the left and right lying portion
			leftPortion := DateRange{start: baseDateRanges[0].start, end: subtractDateRanges[0].start}
			rightPortion := DateRange{start: subtractDateRanges[0].end, end: baseDateRanges[0].end}

			// the 2 new ranges needs to replace the original 1st range on the base list for further processing
			var tempPrepend []DateRange
			if !leftPortion.start.Equal(leftPortion.end) {
				tempPrepend = append(tempPrepend, leftPortion)
			}

			if !rightPortion.start.Equal(rightPortion.end) {
				tempPrepend = append(tempPrepend, rightPortion)
			}

			baseDateRanges = append(tempPrepend, baseDateRanges[1:]...)
			continue
		}
	}

	// if somehow we've exhausted list of ranges to subtract but still have items in base, these can be flushed to the result
	if len(subtractDateRanges) == 0 && len(baseDateRanges) > 0 {
		Result = append(Result, baseDateRanges...)
	}

	return Result
}
